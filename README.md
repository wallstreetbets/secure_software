# Welcome to GME🚀🚀

## How to run
```
FONTEND: CD into the client folder and read the readme there.
BACKEND: Run the SecureSoftwareApplication.java file in src/main/java/software
```

### Working flight data for you to try!
There are about 5000 available flights in our database (randomly generated) but since
there are a lot of flights and countries and dates to choose from,
it may be hard to find one that suits. Here are a few ones you can try.
```
-----------------------
Flight from: Cayman Islands
Flight to: Togo
Date: 2021-11-17
-----------------------
Flight from: Canada
Flight to: Malawi
Date: 2021-12-30
-----------------------
Flight from: Norway
Flight to: Yugoslavia
Date: 2021-04-02
-----------------------
Flight from: South Africa
Flight to: Venezuela
Date: 2021-06-30
-----------------------
Flight from: Cayman Islands
Flight to: Togo
Date: 2021-11-17
-----------------------
Flight from: Portugal
Flight to: Luxembourg
Date: 2021-07-22
-----------------------
You are free to try your own destinations and dates
but due to Covid, we might not have the flights that
suit you :(
```

### How to access our Persistent H2 database
Only access this if you really need too. We weren't to sure if 
you needed access so, we just decided to allow it in case.
```
1. Go to http://localhost:8080/h2-console/
2. Sign in with the following
    Driver Class:   org.h2.Driver
    JDBC URL:       jdbc:h2:file:./mainDB
    User Name:      LLama99gme
    Password:       5fGvxadx5Gud
```

### Admin Details
email: admin@gmail.com  
password: admin

### Password
digit + lowercase char + uppercase char + punctuation + symbol

### API Endpoints
See [API Documentation](https://documenter.getpostman.com/view/12117927/TW76CQ58) if needed.

We left some vulnerabilities for you to find ;)