import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: null
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    }
  },
  actions: {
    setUserAction({ commit }, user) {
      commit("setUser", user);
    }
  },
  getters: {
    user: state => state.user
  }
});
