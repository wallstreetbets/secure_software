import Vue from "vue";
import { extend, localize } from "vee-validate";
import {
  digits,
  required,
  regex,
  confirmed,
  numeric,
  alpha,
  alpha_dash,
  alpha_spaces
} from "vee-validate/dist/rules";
import en from "vee-validate/dist/locale/en.json";

extend("required", required);

extend("customPhoneRegex", {
  validate(value, args) {
    value = value.replace(/\s/g, "");
    return args.phoneRegex.test(value);
  },
  params: ["phoneRegex"]
});

extend("regex", regex);

extend("alpha_dash", alpha_dash);

extend("confirmed", confirmed);

extend("numeric", numeric);

extend("alpha", alpha);

extend("alpha_spaces", alpha_spaces);

extend("digits", digits);

// Install English and Arabic localizations.
localize({
  en: {
    messages: en.messages,
    names: {
      regex: "Regex",
      required: "Required",
      confirmed: "Confirm password",
      numeric: "Numeric",
      alpha: "Alpha",
      alpha_dash: "Alpha_dash",
      customPhoneRegex: "Custom Phone Regex",
      alpha_spaces: "Alpha_spaces",
      digits: "Digits"
    },
    fields: {
      confirmPassword: {
        confirmed: "Passwords don't match",
        required: "Confirm Password field is required"
      },
      phoneNumber: {
        customPhoneRegex: "Invalid number",
        required: "Phone number is required"
      }
    }
  }
});

let LOCALE = "en";

// A simple get/set interface to manage our locale in components.
// This is not reactive, so don't create any computed properties/watchers off it.
Object.defineProperty(Vue.prototype, "locale", {
  get() {
    return LOCALE;
  },
  set(val) {
    LOCALE = val;
    localize(val);
  }
});
