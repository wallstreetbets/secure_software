package software.models;

import javax.persistence.*;
import javax.persistence.Entity;

@Entity
public class CreditCard {
    @Id @GeneratedValue(strategy= GenerationType.IDENTITY) Long card_id;
    private Long userID;
    private String cardholderName;
    private String personalAccessNumber;
    private String expirationDate;

    public Long getCard_id() {
        return card_id;
    }

    public void setCard_id(Long card_id) {
        this.card_id = card_id;
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public String getCardholderName() {
        return cardholderName;
    }

    public void setCardholderName(String cardholderName) {
        this.cardholderName = cardholderName;
    }

    public String getPersonalAccessNumber() {
        return personalAccessNumber;
    }

    public void setPersonalAccessNumber(String personalAccessNumber) {
        this.personalAccessNumber = personalAccessNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }
}