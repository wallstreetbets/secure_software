package software.repositories;

import org.springframework.data.repository.CrudRepository;
import software.models.FlightInfo;

import java.util.Date;
import java.util.List;

/**
 * This will be AUTO IMPLEMENTED by Spring into a Bean called eventRepository
 * CRUD refers Create, Read, Update, Delete
 */
public interface FlightRepository extends CrudRepository<FlightInfo, Long> {
    List<FlightInfo> findByFromDestinationAndToDestinationAndDateDeparture(
            String fromDestination, String toDestination,
            Date dateDeparture);
}