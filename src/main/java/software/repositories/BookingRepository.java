package software.repositories;

import org.springframework.data.repository.CrudRepository;
import software.models.Booking;
import java.util.Optional;
import java.util.List;

/**
 * This will be AUTO IMPLEMENTED by Spring into a Bean called eventRepository
 * CRUD refers Create, Read, Update, Delete
 */
public interface BookingRepository extends CrudRepository<Booking, Long> {
    List<Booking> findByUserID(Long userID);
    Optional<Booking> findByUserIDAndFlightID(Long userID, Long flightID);
}