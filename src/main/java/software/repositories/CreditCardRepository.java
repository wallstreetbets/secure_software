package software.repositories;

import org.springframework.data.repository.CrudRepository;
import software.models.CreditCard;

import java.util.List;
import java.util.Optional;

/**
 * This will be AUTO IMPLEMENTED by Spring into a Bean called eventRepository
 * CRUD refers Create, Read, Update, Delete
 */
public interface CreditCardRepository extends CrudRepository<CreditCard, Long> {
    Optional<CreditCard> findByPersonalAccessNumber(String personalAccessNumber);
    List<CreditCard> findByUserID(Long userID);
}