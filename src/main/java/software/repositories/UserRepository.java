package software.repositories;

import org.springframework.data.repository.CrudRepository;
import software.models.User;

/**
 * This will be AUTO IMPLEMENTED by Spring into a Bean called eventRepository
 * CRUD refers Create, Read, Update, Delete
 */
public interface UserRepository extends CrudRepository<User, Long> {
    User findByEmail(String email);
}