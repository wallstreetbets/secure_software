package software.controller;

// Import log4j classes.
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import software.models.User;
import software.repositories.UserRepository;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
@RequestMapping(value="/api/user")
@Controller
public class UserController {

    private static final Logger logger = LogManager.getLogger(UserController.class);
    
    @Autowired
    private HttpServletRequest request;


    @Autowired
    private LoginAttemptService loginAttemptService;
    
    @Autowired
    private UserRepository userRepository;

    // digit + lowercase char + uppercase char + punctuation + symbol
    private static final String PASSWORD_PATTERN =
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$";
    private static final Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
    
    private static final String EMAIL_PATTERN =
    "^((([!#$%&'*+\\-/=?^_`{|}~\\w])|([!#$%&'*+\\-/=?^_`{|}~\\w][!#$%&'*+\\-/=?^_`{|}~\\.\\w]{0,}[!#$%&'*+\\-/=?^_`{|}~\\w]))[@]\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)$";
    private static final Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);

    private static final String DATE_PATTERN =
        "^(\\d{4})\\D?(0[1-9]|1[0-2])\\D?([12]\\d|0[1-9]|3[01])?$";
    private static final Pattern datePattern = Pattern.compile(DATE_PATTERN);

    private static final String PHONE_PATTERN =
    "\\+(9[976]\\d|8[987530]\\d|6[987]\\d|5[90]\\d|42\\d|3[875]\\d|2[98654321]\\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\\W*\\d\\W*\\d\\W*\\d\\W*\\d\\W*\\d\\W*\\d\\W*\\d\\W*\\d\\W*(\\d{1,2})$";
    private static final Pattern phonePattern = Pattern.compile(PHONE_PATTERN);

    private static final String ADDRESS_PATTERN =
    "^[\\.a-zA-Z0-9,!? ]*$";
    private static final Pattern addressPattern = Pattern.compile(ADDRESS_PATTERN); 

    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    @RequestMapping(value="/test-connection", method=RequestMethod.GET)
    public @ResponseBody String test(){
        return "Working";
    }

   /**
    *  Add user list to database
    */
   @RequestMapping(value="/list", method=RequestMethod.POST)
   public @ResponseBody String addListUser(@RequestBody List<User> user){
       try{
           for(User f : user){
               userRepository.save(f);
           }

           return "success";
       }catch (Exception e){
           return "User couldn't be saved";
       }
   }

    @RequestMapping(value="", method=RequestMethod.POST)
    public @ResponseBody Object addUser(@RequestBody User user){

        if(userRepository.findByEmail(user.getEmail()) == null){

            if(passwordIsValid(user.getPassword())){
                try{
                    String HashedPassword = new String(new BCryptPasswordEncoder().encode(user.getPassword()));
                    user.setPassword(HashedPassword);
                    userRepository.save(user);
                    logger.info("User " + user.getUser_id() + " has been successfully created with email "+user.getEmail());
                    return userRepository.findByEmail(user.getEmail()).getUser_id();
                }catch (Exception e){
                    logger.error("User " + user.getUser_id() + " could not be created");
                    return "User couldn't be saved";
                }
            }else{
                return "Password not strong enough";
            }


        }else{
            return "Email exists";
        }
    }
    
    /**
     *  Get all available User or by ID
     */
    @RequestMapping(value="", method=RequestMethod.GET)
    public @ResponseBody
    Object getBooking(@RequestParam(required = false) Long user_id){
        if(user_id != null){
            try{
                return userRepository.findById(user_id);
            }catch (Exception e){
                logger.error("User id: " + user_id + " couldn't be found.");
                return "User id: " + user_id + " couldn't be found.";
            }
        }else{
            return userRepository.findAll();
        }
    }

    /**
     *  Remove User by ID
     */
    @RequestMapping(value="", method=RequestMethod.DELETE)
    public @ResponseBody
    String removeUser(@RequestParam(required = true) Long user_id){
        try{
            userRepository.deleteById(user_id);
            logger.info("User " + user_id + " has been removed.");
            return "success";
        }catch (Exception e){
            logger.error("User id: " + user_id + " couldn't be found.");
            return "User id: " + user_id + " couldn't be found.";
        }
    }

    /**
     *  Update user details by user_id
     */
    @RequestMapping(value="/update", method=RequestMethod.POST)
    public @ResponseBody
    String updateUserDetails(@RequestBody User userDetails, HttpServletRequest request){
        long user_id;
        if(request.getSession(false) != null){
            Long uid = (Long) request.getSession().getAttribute("uid");
            if(uid == null){
                return "Session inactive";
            }
            user_id = uid;
        }else{
            return "Session inactive";
        }
        return updateUser(user_id, userDetails);
    }
    
    public String updateUser(long user_id, User userDetails){
        if(user_id > -1 && userRepository.findById(user_id).isPresent()){
            try{
                User user = userRepository.findById(user_id).get();
                if(!addressIsValid(userDetails.getAddress())){
                    return "Error";
                }
                if(!phoneIsValid(userDetails.getPhoneNumber())){
                    return "Error";
                }
                if(!emailIsValid(userDetails.getEmail())){
                    return "Error";
                }
                if(!dateIsValid(userDetails.getDob().toString())){
                    return "Error";
                }
                if(userDetails.getFirstName().isEmpty()){
                    return "Error";
                }
                if(userDetails.getSurname().isEmpty()){
                    return "Error";
                }
                if(userDetails.getFirstName() != null){user.setFirstName(userDetails.getFirstName());}
                if(userDetails.getSurname() != null){user.setSurname(userDetails.getSurname());}
                if(userDetails.getEmail() != null){user.setEmail(userDetails.getEmail());}
                if(userDetails.getPassword() != null){
                    if(passwordIsValid(user.getPassword())){
                        String HashedPassword = new String(new BCryptPasswordEncoder().encode(userDetails.getPassword()));
                        user.setPassword(HashedPassword);
                    }else{
                        return "Password not strong enough";
                    }
                }
                if(userDetails.getPhoneNumber() != null){user.setPhoneNumber(userDetails.getPhoneNumber());}
                if(userDetails.getAddress() != null){user.setAddress(userDetails.getAddress());}
                if(userDetails.getDob() != null){user.setDob(userDetails.getDob());}
                user.setGuest(false);

                userRepository.save(user);
                logger.info("User " + user_id + " has been updated.");
                return "success";
            }catch (Exception e){
                logger.error("Unable to update user information. Unknown exception");
                return "Unable to update user information.";
            }
        }else {
            return "User id: " + user_id + " couldn't be found.";
        }
    }
    @RequestMapping(value="/updatepassword", method=RequestMethod.POST)
    public @ResponseBody
    String updatePassword(@RequestParam String oldPassword,@RequestParam String newPassword, HttpServletRequest request){
        long user_id;
        if(request.getSession(false) != null){
            Long uid = (Long) request.getSession().getAttribute("uid");
            if(uid == null){
                return "Session inactive";
            }
            user_id = uid;
        }else{
            return "Session inactive";
        }
        if(user_id > -1 &&userRepository.findById(user_id).isPresent()){
            try{
                User user = userRepository.findById(user_id).get();
                if(passwordIsValid(newPassword)){
                        if(encoder.matches(oldPassword, user.getPassword())){
                            String HashedPassword = new String(new BCryptPasswordEncoder().encode(newPassword));
                            user.setPassword(HashedPassword);
                        } else {
                            return "Old password incorrect";
                        }
                    }else{
                        return "Password not strong enough";
                    }
                user.setGuest(false);

                userRepository.save(user);
                logger.info("User " + user_id + " has been updated.");
                return "success";
            }catch (Exception e){
                logger.error("Unable to update user information. Unknown exception");
                return "Unable to update user information.";
            }
        }else {
            return "User id: " + user_id + " couldn't be found.";
        }
    }
    @RequestMapping(value="/sign-up", method=RequestMethod.POST)
    public @ResponseBody String signup(@RequestBody User user, HttpSession session){
        if(!passwordIsValid(user.getPassword())){
            return "Password Invalid";
        }
        if(!addressIsValid(user.getAddress())){
            return "Error";
        }
        if(!phoneIsValid(user.getPhoneNumber())){
            return "Error";
        }
        if(!emailIsValid(user.getEmail())){
            return "Error";
        }
        if(!dateIsValid(user.getDob().toString())){
            return "Error";
        }
        if(user.getFirstName().isEmpty()){
            return "Error";
        }
        if(user.getSurname().isEmpty()){
            return "Error";
        }
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        if(userRepository.findByEmail(user.getEmail())!= null){
                        if(userRepository.findByEmail(user.getEmail()).getGuest()){
                long userID = userRepository.findByEmail(user.getEmail()).getUser_id();
                updateUser(userID, user);
                logger.info("User " + userID + " has signed up to exec club.");
                return "Account upgraded from user to Exec";
            }else{
                return "Account with this email already exists";
            }

        }else{
            userRepository.save(user);
            logger.info("User " + user.getUser_id() + " has signed up to exec club.");
            return "Account created";
        }
    }
    
    @RequestMapping(value="/sign-in", method=RequestMethod.POST)
    public @ResponseBody
    Object signin(@RequestBody User user, HttpSession session){
        String ip = request.getRemoteAddr();
        if(loginAttemptService.isBlocked(ip)){
            return "This IP has been blocked for too many failed login attemps, try again in 20 minutes";
        } else if(userRepository.findByEmail(user.getEmail()) != null && encoder.matches(user.getPassword(), userRepository.findByEmail(user.getEmail()).getPassword())){
            loginAttemptService.loginSucceeded(ip);
            Long uid = userRepository.findByEmail(user.getEmail()).getUser_id();
            session.setAttribute("uid", uid);
            logger.info("User " + uid + " has signed in.");
            return userRepository.findById(uid);
        }else{
            loginAttemptService.loginFailed(ip);
            return "Account with given credentials does not exist";
        }
    }

    @RequestMapping(value="/statusSession", method=RequestMethod.POST)
    public @ResponseBody
    Object statusSession(HttpServletRequest request) {
        if(request.getSession(false) != null){
            Long uid = (Long) request.getSession().getAttribute("uid");
            if(uid == null){
                return "Session inactive";
            }
            return userRepository.findById(uid);
        }else{
            return "Session inactive";
        }
    }

    @RequestMapping(value="/destroySession", method=RequestMethod.POST)
    public @ResponseBody
    Object destroySession(HttpServletRequest request) {
        if(request.getSession(false) != null){
            request.getSession().invalidate();
            return "Session destroyed";
        }else{
            return "Invalid";
        }
    }

    //Checks if password is valid (REGEX check)
    public static boolean passwordIsValid(final String password) {
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }
    public static boolean emailIsValid(final String email) {
        Matcher matcher = emailPattern.matcher(email);
        return matcher.matches();
    }
    public static boolean phoneIsValid(final String phone) {
        Matcher matcher = phonePattern.matcher(phone);
        return matcher.matches();
    }
    public static boolean addressIsValid(final String address) {
        Matcher matcher = addressPattern.matcher(address);
        return matcher.matches();
    }
    public static boolean dateIsValid(final String date) {
        Matcher matcher = datePattern.matcher(date);
        return matcher.matches();
    }

}
