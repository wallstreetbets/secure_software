package software.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import software.models.Booking;
import software.models.FlightInfo;
import software.repositories.BookingRepository;
import software.repositories.FlightRepository;

import java.time.Instant;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
@RequestMapping(value="/api/booking")
@Controller
public class BookingController {

    private static final Logger logger = LogManager.getLogger(UserController.class);

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private FlightRepository flightRepository;
    
    @RequestMapping(value="/test-connection", method= RequestMethod.GET)
    public @ResponseBody
    String test(){
        return "Working";
    }

   /**
     *  Add Booking to database
     */
    @RequestMapping(value="", method=RequestMethod.POST)
    public @ResponseBody Object addBooking(@RequestBody Booking booking){

        try{
            booking.setStatus("Scheduled");
            bookingRepository.save(booking);
            logger.info("Booking "+ booking.getBooking_id() + " saved");
            return bookingRepository.save(booking).getBooking_id();
        }catch (Exception e){
            logger.error("Booking "+ booking.getBooking_id() + " couldn't be saved");
            return "Booking couldn't be saved";
        }
    }

    /**
     *  Get all available Bookings or by ID
     */
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value="", method=RequestMethod.GET)
    public @ResponseBody
    Object getBooking(@RequestParam(required = false) Long booking_id, HttpServletRequest request){

        List<Optional<FlightInfo>> flightInfo = new ArrayList<>();
        if(request.getSession(false) != null){
            Long uid = (Long) request.getSession().getAttribute("uid");

            if(booking_id != null){
                try{
                    return flightRepository.findById(bookingRepository.findById(booking_id).get().getFlightID());
                }catch (Exception e){
                    return "Booking id: " + booking_id + " couldn't be found.";
                }
            }else if(uid != null){
                try{
                    Iterable<Booking> bookings = bookingRepository.findByUserID(uid);

                    for(Booking booking : bookings){


                        checkStatusPast(booking);
                        flightInfo.add(flightRepository.findById(booking.getFlightID()));
                    }

                    return flightInfo;
                }catch (Exception e){
                    return "User id: " + uid + " couldn't be found.";
                }
            } else{
                return bookingRepository.findAll();
            }
        }else{
            return "Session inactive";
        }
        
    }

    /**
     * Check if the current date is past check in day
     * @param booking
     */
    private void checkStatusPast(Booking booking) {

        Long flightID = booking.getFlightID();

        long ut1 = Instant.now().getEpochSecond();
        long ut2 = flightRepository.findById(flightID).get().getCheckInDay().getTime() / 1000L;

        if(ut1 > ut2 && !booking.getStatus().equals("Past")){
            booking.setStatus("Past");
        }
    }

    /**
     * Remove Booking by ID
     */
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value="", method=RequestMethod.DELETE)
    public @ResponseBody
    Object removeBooking(@RequestParam(required = false) Long booking_id, HttpServletRequest request, @RequestParam(required = false) Long flightID){

        if(request.getSession(false) != null){
            Long uid = (Long) request.getSession().getAttribute("uid");
            if(uid == null){
                return "Session inactive";
            }
            if(uid != null && flightID != null){
                try{
                    Long id = bookingRepository.findByUserIDAndFlightID(uid, flightID).get().getBooking_id();
                    bookingRepository.deleteById(id);
                    logger.info("Booking "+ booking_id + " removed");
                    return "success";
                }catch(Exception e){
                    logger.error("Unable to delete flight Booking "+ booking_id);
                    return "Unable to delete flight";
                }
            }else{
                try{
                    Long flight_id = bookingRepository.findById(booking_id).get().getFlightID();

                    long ut1 = Instant.now().getEpochSecond();
                    long ut2 = flightRepository.findById(flight_id).get().getCheckInDay().getTime() / 1000L;

                    long diff = ut2 - ut1;
                    if(diff < 86400){
                        return "Cannot cancel reservation. Flight leaving in under 24 hours";
                    }else{
                        bookingRepository.deleteById(booking_id);
                        return "success";
                    }
                }catch (Exception e){
                    logger.error("Unable to delete flight Booking "+ booking_id+" booking couldn't be found");
                    return "Booking id: " + booking_id + " couldn't be found.";
                }
            }
        }else{
            return "Session inactive";
        }
    }
}
