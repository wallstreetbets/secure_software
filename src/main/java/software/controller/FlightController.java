package software.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import software.models.FlightInfo;
import software.repositories.FlightRepository;
import java.text.SimpleDateFormat;  
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
@RequestMapping(value="/api/flight")
@Controller
public class FlightController {

    private static final Logger logger = LogManager.getLogger(UserController.class);

    @Autowired
    private FlightRepository flightRepository;
    
    @RequestMapping(value="/test-connection", method=RequestMethod.GET)
    public @ResponseBody String test(){
        return "Working";
    }

    /**
     *  Add flights list to database
     */
    @RequestMapping(value="/list", method=RequestMethod.POST)
    public @ResponseBody String addListFlight(@RequestBody List<FlightInfo> flight){
        try{
            for(FlightInfo f : flight){
                flightRepository.save(f);
            }

            return "success";
        }catch (Exception e){
            return "Flight couldn't be saved";
        }
    }

    /**
     *  Add flight to database
     */
    @RequestMapping(value="", method=RequestMethod.POST)
    public @ResponseBody String addFlight(@RequestBody FlightInfo flight){
        try{
            flightRepository.save(flight);
            logger.info("Flight " + flight.getFlight_id() + " added to database");
            return "success";
        }catch (Exception e){
            logger.error("Flight " + flight.getFlight_id() + " couldn't be saved");
            return "Flight couldn't be saved";
        }
    }

    /**
     *  Get all available flights or by ID
     */
    @RequestMapping(value="", method=RequestMethod.GET)
    public @ResponseBody
    Object getFlight(@RequestParam(required = false) Long flight_id){
        if(flight_id != null){
            try{
                return flightRepository.findById(flight_id);
            }catch (Exception e){
                logger.error("Flight " + flight_id + " couldn't be found");
                return "Flight id: " + flight_id + " couldn't be found.";
            }
        }else{
            return flightRepository.findAll();
        }
    }

    /**
     *  Get all available flights from input search
     */
    @RequestMapping(value="/search", method=RequestMethod.GET)
    public @ResponseBody
    Object getFlightByDate(@RequestParam(required = true) String fromDestination,@RequestParam(required = true) String toDestination,@RequestParam(required = true) String dateDeparture){
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date convertedDate = sdf.parse(dateDeparture);
            return flightRepository.findByFromDestinationAndToDestinationAndDateDeparture(
                    fromDestination, toDestination,
                    convertedDate);
        }catch (Exception e){
            return "No flights available";
        }
    }

    /**
     *  Remove flight by ID
     */
    @RequestMapping(value="", method=RequestMethod.DELETE)
    public @ResponseBody
    String removeFlight(@RequestParam(required = true) Long flight_id){
        try{
            flightRepository.deleteById(flight_id);
            logger.info("Flight " + flight_id + " has been removed from database");
            return "success";
        }catch (Exception e){
            logger.error("Flight " + flight_id + " couldn't be found");
            return "Flight id: " + flight_id + " couldn't be found.";
        }
    }


}
