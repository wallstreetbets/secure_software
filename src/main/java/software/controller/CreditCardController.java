package software.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import software.models.CreditCard;
import software.repositories.CreditCardRepository;

@CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
@RequestMapping(value="/api/payment")
@Controller
public class CreditCardController {

    private static final Logger logger = LogManager.getLogger(UserController.class);
    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    @Autowired
    private CreditCardRepository creditCardRepository;

    @RequestMapping(value="/test-connection", method= RequestMethod.GET)
    public @ResponseBody
    String test(){
        return "Working";
    }
    public @ResponseBody Object addPaymentDetails(@RequestBody CreditCard paymentDetails, HttpServletRequest request){
        if(request.getSession(false) != null){
            Long uid = (Long) request.getSession().getAttribute("uid");
            if(uid == null){
                return "Session inactive";
            }
            try{
                char[] expirationDate = paymentDetails.getExpirationDate().toCharArray();
            if(paymentDetails.getPersonalAccessNumber().length() != 16){
                return "Payment details couldn't be saved";
            }
            if(paymentDetails.getCardholderName().isEmpty()){
                return "Payment details couldn't be saved";
            }
            if(paymentDetails.getExpirationDate().length()!=4){
                return "Payment details couldn't be saved";
            }
            if (Character.getNumericValue(expirationDate[3]) < 2) {
                return "Payment details couldn't be saved";
              }
              if (Character.getNumericValue(expirationDate[3]) == 2 && Character.getNumericValue(expirationDate[4]) == 0) {
                return "Payment details couldn't be saved";
              }
              if (
                Character.getNumericValue(expirationDate[3])== 2 &&
                Character.getNumericValue(expirationDate[4]) == 1 &&
                Character.getNumericValue(expirationDate[0]) == 0 &&
                Character.getNumericValue(expirationDate[1]) <= 5
              ) {
                return "Payment details couldn't be saved";
              }
                if(creditCardRepository.findByPersonalAccessNumber(paymentDetails.getPersonalAccessNumber()).isPresent()){
                    logger.error("Payment details couldn't be saved");
                    return "Payment details couldn't be saved";
                }else{
                    logger.info("Payment details saved");
                    String HashedString = new String(new BCryptPasswordEncoder().encode(paymentDetails.getCardholderName()));
                    paymentDetails.setCardholderName(HashedString);
                    HashedString = new String(new BCryptPasswordEncoder().encode(paymentDetails.getPersonalAccessNumber()));
                    paymentDetails.setPersonalAccessNumber(HashedString);

                    return creditCardRepository.save(paymentDetails).getCard_id();
                }
            }catch (Exception e){
                return "Payment details couldn't be saved";
            }
        }else{
            return "Session inactive";
        }
    }

    /**
     *  Get all Credit Card info by user id
     */
    @RequestMapping(value="", method=RequestMethod.GET)
    public @ResponseBody
    Object getCreditCards(HttpServletRequest request){
        if(request.getSession(false) != null){
            Long uid = (Long) request.getSession().getAttribute("uid");
            if(uid == null){
                return "Session inactive";
            }else{
                try{
                    return creditCardRepository.findByUserID(uid);
                }catch (Exception e){
                    return "User id: " + uid + " couldn't be found.";
                }
            }  
        }else{
            return "Session inactive";
        }
    }

    /**
     *  Remove payment details by creditCard id
     */
    @RequestMapping(value="", method=RequestMethod.DELETE)
    public @ResponseBody
    String removePaymentDetails(@RequestParam(required = true) Long card_id, HttpServletRequest request){
        
        if(request.getSession(false) != null){
            Long uid = (Long) request.getSession().getAttribute("uid");
            if(uid == null){
                return "Session inactive";
            }
            
            
            try{
                logger.info("Card " + card_id + " removed");
                creditCardRepository.deleteById(card_id);
                return "success";
            }catch(Exception e){
                logger.error("Card " + card_id + " could not be removed");
                return "Card id: " + card_id + " couldn't be found.";
            }

        
        }else{
            return "Session inactive";
        }
        
    }

    /**
     *  Update payment details by creditCard id
     */
    @RequestMapping(value="/update", method=RequestMethod.POST)
    public @ResponseBody
    String updatePaymentDetails(@RequestParam(required = true) Long card_id, @RequestBody CreditCard paymentDetails, HttpServletRequest request){

        if(creditCardRepository.findById(card_id).isPresent()){
            try{
                CreditCard card = creditCardRepository.findById(card_id).get();
                if(paymentDetails.getPersonalAccessNumber()!= null){
                    String HashedString = new String(new BCryptPasswordEncoder().encode(paymentDetails.getPersonalAccessNumber()));
                    paymentDetails.setPersonalAccessNumber(HashedString);
                    card.setPersonalAccessNumber(paymentDetails.getPersonalAccessNumber());
                }
                if(paymentDetails.getCardholderName()!= null){
                    String HashedString = new String(new BCryptPasswordEncoder().encode(paymentDetails.getCardholderName()));
                    paymentDetails.setCardholderName(HashedString);
                    card.setCardholderName(paymentDetails.getCardholderName());
                }
                if(paymentDetails.getExpirationDate()!= null){card.setExpirationDate(paymentDetails.getExpirationDate());}
                logger.info("Card " + card_id + " updated");
                creditCardRepository.save(paymentDetails);

                return "success";
            }catch (Exception e){
                logger.error("Unable to update payment information. Card " + card_id);
                return "Unable to update payment information.";
            }
        }else {
            return "Card id: " + card_id + " couldn't be found.";
        }
    }
}
